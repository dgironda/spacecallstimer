﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class VideoControlManager : MonoBehaviour {

	public MediaPlayerCtrl videoSpaceCalls;
	public Slider scrubSeek;

	// Use this for initialization
	void Start(){

	}

	void OnEnable () {
		scrubSeek.value = videoSpaceCalls.GetSeekBarValue();
		scrubSeek.onValueChanged.AddListener(ChangeValue);
	}

	void Update(){
//		scrubSeek.value = videoSpaceCalls.GetSeekBarValue();
	}

	public void StopButton(){
		SceneManager.LoadScene (0);
	}

	public void PauseButton(){
		videoSpaceCalls.Pause();
	}

	public void PlayButton(){
		videoSpaceCalls.Play ();
	}
		

	void ChangeValue(float value)
	{
		videoSpaceCalls.SetSeekBarValue (value);
	}

}
