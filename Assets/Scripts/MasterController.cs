﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MasterController : MonoBehaviour {

	//get Debug Menu from canvas
	public GameObject debugMenu;

	//get Canvas to turn off all child panels
	public GameObject canvas;

	//get video
	public MediaPlayerCtrl videoSpaceCalls;
	private bool startFilm;

	//star button to be hidden
	public GameObject videoButton;

	//password button combo
	private string[] debugPassword = new string[5] {"Up", "Up", "Down", "Up", "Up"};

	private int debugPwIndex = 0;
	private const int DEBUG_PASSWORD_LENGTH = 5;


	// Use this for initialization
	void Start ()
	{
		startFilm = true;
//		this.videoSpaceCalls.OnEnd += this.OnVideoEnd;
	}

	//hidden button on screen to start film, does nothing once the video is played
	public void StartFilmButton()
	{
		if (startFilm == true)
		{
			videoSpaceCalls.Play ();
			this.StartCoroutine (WaitTwoSecondsThenStart());
		}
	}

	private void OnVideoEnd(){
		SceneManager.LoadScene (0);
	}



	private IEnumerator WaitTwoSecondsThenStart()
	{
		yield return new WaitForSeconds (2f);


		startFilm = false;
		videoButton.SetActive (false);

	}






	/// <summary>
	/// Hidden on screen buttons pressed in order bring up the Main Menu
	/// </summary>
	public void ShowDebugMenu(string buttonInput)
	{
		Debug.Log (buttonInput);

		//Check buttonInput against debugPassword
		if (debugPassword [debugPwIndex] == buttonInput)
		{
			//If true, increase the password index
			debugPwIndex++;

			//When the password index equals the length of the password
			if (debugPwIndex >= DEBUG_PASSWORD_LENGTH)
			{
				//Reset index
				debugPwIndex = 0;

				//Turn off all child elements that do not have the tag "DebugButton"
				foreach (Transform child in canvas.transform)
				{
					GameObject childPanel = child.gameObject;

					if (childPanel.tag != "DebugButton") {
						childPanel.SetActive (false);
					}
				}

				//Turn on debug menu
				debugMenu.SetActive (true);
			}
		}
		else
		{
			//If false, reset password index to 0
			debugPwIndex = 0;
		}

		//Reset password index
		if (buttonInput == "Clear")
		{
			debugPwIndex = 0;

		}
	
	}


}
