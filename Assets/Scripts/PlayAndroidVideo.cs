﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAndroidVideo : MonoBehaviour {
	public string videoPath;

	// Use this for initialization
	void Start () {
		Handheld.PlayFullScreenMovie(videoPath, Color.black, FullScreenMovieControlMode.Full, FullScreenMovieScalingMode.AspectFill);
	}

}
