﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugMenuButton : MonoBehaviour {
	public string videoPath;

	// Use this for initialization
	public void OnClickDebugButton() {
		Handheld.PlayFullScreenMovie(videoPath, Color.black, FullScreenMovieControlMode.Full, FullScreenMovieScalingMode.AspectFill);
	}

}
